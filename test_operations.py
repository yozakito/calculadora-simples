from secondgrau import second_function
from operacoes import divisao, subtractFromList, multiplicacao, logaritmo, subtracao, orderList, factorial


def test_div():
    assert divisao(4, 2) == 2
    assert divisao(4, 2) != 1


def test_logaritmo():
    assert logaritmo(1000) == 6.907755278982137
    assert logaritmo(100, 2) == 6.643856189774725


def test_subtracao_lista_positiva():
    assert subtractFromList([1, 2, 3], 1) == [0, 1, 2]


def test_subtracao_lista_negativa():
    assert subtractFromList([-1, -2, -3], 1) == [-2, -3, -4]


def test_ordernar_lista_ordenada():
    assert orderList([1, 2, 3]) == [1, 2, 3]


def test_ordernar_lista_desordenada():
    assert orderList([3, 2, 1]) == [1, 2, 3]


def test_ordernar_lista_numeros_repetidos():
    assert orderList([3, 2, 1, 3, 2, 1]) == [1, 1, 2, 2, 3, 3]


def test_multiplicacao():
    assert multiplicacao(2, 2) == 4
    assert multiplicacao(2, 4) != 4


def test_subtracao():
    assert subtracao(4, 3) == 1
    assert subtracao(4, 4) != 4

def test_factorial():
    assert factorial(5) == [120]

def test_polinomio_x_positivo():
    # x**2 + 3 * x + 2
    equation = second_function(1, 3, 2)

    assert equation.resolveFunction(2) == 12
    assert equation.resolveFunction(1) == 6

def test_polinomio_x_negativo():
    equation = second_function(1, 3, 2)

    assert equation.resolveFunction(-2) == 0.0
    assert equation.resolveFunction(-1) == 0.0

def test_polinomio_x_igual_zero():
    equation = second_function(1, 3, 2)

    assert equation.resolveFunction(0) == 2
