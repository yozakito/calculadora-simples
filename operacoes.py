import math


def orderList(numberList):
    numberList.sort()
    return numberList


def factorial(*args):
    respostas = []
    for valor in args:
        resultado=1
        count=1
        if isinstance(int(valor), int):
            while count <= valor:
                resultado *= count
                count += 1
            respostas.append(resultado)
    return respostas


def subtractFromList(listValues, numberToSubtract):
    for i in range(len(listValues)):
        listValues[i] = listValues[i] - numberToSubtract

    return listValues


def adicao(num1, num2):
    return num1 + num2


def subtracao(num1, num2):
    return num1 - num2


def multiplicacao(num1, num2):
    return num1 * num2


def divisao(num1, num2):
    if num2 == 0:
        return "Erro: divisão por zero."
    return num1 / num2


def logaritmo(numero, base_input=''):
    if base_input == '':
        return math.log(numero)
    else:
        base = float(base_input)
        return math.log(numero, base)
