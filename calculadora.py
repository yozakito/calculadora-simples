import math
from secondgrau import second_function
import os 


def orderList():
    print('Informe os numeros da sua lista (p - parar):')
    numberList = list()
    while True:
        number = input()

        if number == 'p':
            break

        else:
            try:
                number = int(number)
                numberList.append(number)
            except ValueError:
                print('Informe apenas numeros inteiros')

    numberList.sort()
    return numberList


def factorial(*args):
    respostas = []
    for valor in args:
        resultado = 1
        count = 1
        if isinstance(int(valor), int):
            while count <= valor:
                resultado *= count
                count += 1
            respostas.append(resultado)
    return respostas


def subtractFromList():
    print('Informe os numeros da sua lista (p - parar):')
    numberList = list()

    while True:
        number = input()

        if number == 'p':
            break

        else:
            try:
                number = int(number)
                numberList.append(number)
            except ValueError:
                print('Informe apenas numeros inteiros')

    while True:
        print("Quanto vai substrair?")
        substract = input()
        try:
            substract = int(substract)
            break
        except ValueError:
            print('Informe apenas numeros inteiros')

    for i in range(len(numberList)):
        numberList[i] = numberList[i] - substract

    return numberList


def adicao(num1, num2):
    return num1 + num2


def subtracao(num1, num2):
    return num1 - num2


def multiplicacao(num1, num2):
    return num1 * num2


def divisao(num1, num2):
    if num2 == 0:
        return "Erro: divisão por zero."
    return num1 / num2


def logaritmo(numero, base_input=''):
    if base_input == '':
        return math.log(numero)
    else:
        base = float(base_input)
        return math.log(numero, base)
    
def generateLog(operation,resultado, valores = []):
    path = "logs.txt"
    file = open(path, "a")
    file.write(f"\noperação => {str(operation)} \n")
    for i in valores:
        file.write(f"valor => {str(i)}\n")

    file.write(f"resultado => {str(resultado)}\n")
    file.write("-"*30)
    file.close()


def calculadora():
    print("# Bem-vindo à Calculadora Matemática #")
    while True:
        print("Escolha uma opção:")
        print("1 - Adição")
        print("2 - Ordernar Lista")
        print("3 - Substrair da Lista")
        print("4 - Divisão")
        print("5 - Fatorial")
        print("6 - Logaritmo")
        print("7 - montar equação")
        print('77 - Mostrar raízes' )
        print('777 - Mostrar delta' )
        print('71 - Resolver equação' )
        print("8 - Sair")

        opcao = input("Digite o número da opção escolhida: ")

        if opcao == '8':
            print("Obrigado por usar a calculadora. Adeus!")
            break

        if (opcao in ['1', '4', '5']):
            num1 = float(input("Digite o primeiro número: "))
            num2 = float(input("Digite o segundo número: "))

        if (opcao in ['7','77','777']):
            num1 = float(input("Digite o Coeficiente A: "))
            num2 = float(input("Digite o Coeficiente B: "))
            num3 = float(input("Digite o Coeficiente C: "))
        if(opcao == '71'):
            valor_x = float(input("Digite o valor de x para calcular o f(x)"))
            num1 = float(input("Digite o Coeficiente A: "))
            num2 = float(input("Digite o Coeficiente B: "))
            num3 = float(input("Digite o Coeficiente C: "))
        if (opcao == '6'):
            numero = float(input("Digite o número para calcular o logaritmo: "))
            base_input = input("Digite a base do logaritmo (ou pressione Enter para logaritmo natural): ")

        if opcao == '1':
            resultado = adicao(num1, num2)
            operacao = "adição"
            generateLog(operacao,resultado,valores=[num1,num2])
        elif opcao == '2':
            resultado = orderList()
            operacao = "Ordenar Lista"
            generateLog(operacao,resultado,valores=[*resultado])
        elif opcao == '3':
            resultado = subtractFromList()
            operacao = "Substrair da Lista"
            generateLog(operacao,resultado,valores=[*resultado])
        elif opcao == '4':
            resultado = divisao(num1, num2)
            operacao = "divisão"
            generateLog(operacao,resultado,valores=[num1,num2])
        elif opcao == '5':
            resultado = factorial(num1, num2)
            operacao = "fatorial"
            generateLog(operacao,resultado,valores=[num1,num2])
        elif opcao == '6':
            resultado = logaritmo(numero, base_input)
            operacao = 'Logaritmo'
            generateLog(operacao,resultado,valores=[numero])
        elif opcao == '7':
            equation = second_function(num1, num2, num3)
            resultado = equation.showEquation()
            operacao = "montar equação"
            generateLog(operacao,resultado,valores=[num1, num2, num3])
        elif opcao == '77':
            equation = second_function(num1, num2, num3)
            r1 = equation.raiz1()
            r2 = equation.raiz2()
            resultado = f'raiz1: {r1} e raiz2 é: {r2}'
            operacao = "montar raízes"
            generateLog(operacao,resultado,valores=[num1, num2, num3])
        elif opcao == '777':
            equation = second_function(num1, num2, num3)
            delta = equation.delta()
            resultado = f'delta: {delta}'
            operacao = "montar delta"
            generateLog(operacao,resultado,valores=[num1, num2, num3])
        elif opcao == '71':
            equation = second_function(num1,num2,num3)
            fx = equation.resolveFunction(valor_x)
            resultado = f'f(x) => {fx}'
            operacao = "resovler equação"
            generateLog(operacao,resultado,valores=[num1, num2, num3])
        else:
            print("Opção inválida. Tente novamente.")
            continue

        print(f"O resultado da {operacao} é: {resultado}")
        
        nova_operacao = input("Deseja realizar outra operação? (sim/não): ").lower()

        if nova_operacao != "sim":
            print("Obrigado por usar a calculadora. Adeus!")
            os.system('clear')  # Linux/macOS 
            os.system('cls')  # Windows
            break


calculadora()



